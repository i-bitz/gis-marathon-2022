# GIS Marathon 2022

> สิ่งที่เราอยากบอกคุณ

1. เอกสารต่าง ๆ ที่ใช้ในกิจกรรม GIS Marathon ถูกทำขึ้นจากความตั้งใจของทุกคนที่มีงานประจำอยู่แล้ว อาจจะมีข้อผิดพลาดอยู่บ้าง จึงขออภัยไว้นะที่นี้ด้วย

2. ถ้าคุณมีจิตใจสาธารณะ เราอนุญาตให้คุณเข้ามาแก้เอกสารของเราให้มีความถูกต้องและครบถ้วนมากขึ้น

3. ถ้านำเอกสารของพวกเราไปใช้ กรุณา ให้เครดิตกันด้วย
